require "features_helper"

RSpec.describe "Visit home" do

  it "is successful" do
    visit "/"
    expect(page.body).to have_content("A Nice Group Of People")
  end

end
