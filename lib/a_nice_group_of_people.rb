require 'lotus/model'
Dir["#{ __dir__ }/a_nice_group_of_people/**/*.rb"].each { |file| require_relative file }

Lotus::Model.configure do
  ##
  # Database adapter
  #
  # Available options:
  #
  #  * Memory adapter
  #    adapter type: :memory, uri: 'memory://localhost/a_nice_group_of_people_development'
  #
  #  * SQL adapter
  #    adapter type: :sql, uri: 'sqlite://db/a_nice_group_of_people_development.sqlite3'
  #    adapter type: :sql, uri: 'postgres://localhost/a_nice_group_of_people_development'
  #    adapter type: :sql, uri: 'mysql://localhost/a_nice_group_of_people_development'
  #
  adapter type: :sql, uri: ENV['A_NICE_GROUP_OF_PEOPLE_DATABASE_URL']

  ##
  # Migrations
  #
  migrations 'db/migrations'
  schema     'db/schema.sql'

  ##
  # Database mapping
  #
  # Intended for specifying application wide mappings.
  #
  # You can specify mapping file to load with:
  #
  # mapping "#{__dir__}/config/mapping"
  #
  # Alternatively, you can use a block syntax like the following:
  #
  mapping do
    # collection :users do
    #   entity     User
    #   repository UserRepository
    #
    #   attribute :id,   Integer
    #   attribute :name, String
    # end
  end
end.load!
